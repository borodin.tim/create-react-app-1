import React, { useState, useContext } from 'react';
import NotesContext from '../context/notes-context';
import useMousePosition from '../hooks/useMousePosition';

const AddNoteForm = () => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const { dispatch } = useContext(NotesContext);
    const position = useMousePosition({ x: 0, y: 0 });

    const addNote = (e) => {
        e.preventDefault();
        // setNotes([
        //   ...notes,
        //   { title, body }
        // ]);
        const note = { title, body };
        dispatch({ type: 'ADD_NOTE', note });
        setTitle('');
        setBody('');
    };

    return (
        <>
            <p>Add note {position.x}, {position.y}</p>
            <form onSubmit={addNote}>
                <p>
                    Title:
                    <input value={title} onChange={(e) => setTitle(e.target.value)} />
                </p>
                <p>
                    Body:
                    <textarea value={body} onChange={(e) => setBody(e.target.value)} />
                </p>
                <button>add note</button>
            </form>
        </>
    );
};

export { AddNoteForm as default };