import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import NoteApp from './components/NoteApp';

ReactDOM.render(<React.StrictMode> <NoteApp /> </React.StrictMode>, document.getElementById('root')
);
reportWebVitals();